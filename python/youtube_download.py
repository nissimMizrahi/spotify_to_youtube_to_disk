import youtube_dl
import os
import requests
from bs4 import BeautifulSoup
import re
import json

import threading



def download_song(url, path = os.getcwd(), filename=""):

    old_wd = os.getcwd()

    ydl_opts = {
        'quiet': True,
        #'verbose': False,
        #'ignoreerrors': True,

        'format': 'bestaudio/best',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '192',
        }],
    }
    if filename != "":
        ydl_opts['outtmpl'] = f'{filename}.mp3'

    os.chdir(path)

    try:
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            ydl.download([url])
    except Exception as e:
        if "sorry" in str(e).lower(): 
            raise Exception("try another one")
    os.chdir(old_wd)


def get_video_hrefs(title):
    words = title.split(" ")
    query = f"https://www.youtube.com/results?search_query={'+'.join(words)}"


    content = requests.get(query).text
    soupHandler = BeautifulSoup(content, features="html.parser")
    
    videos = soupHandler.findAll('div', {'class' : 'yt-lockup-thumbnail contains-addto'})

    for video in videos:
        href = re.findall(r"/watch\?v=([^\"]+)", str(video))
    
        if len(href) == 0: 
            continue

        yield href[0] 

def download_song_by_title(title, path = os.getcwd()):
    links = get_video_hrefs(title)

    done = False
    while not done:
        try:
            link = next(links)
            print(F"downloading {title}: link - {link}")
            download_song(f"https://www.youtube.com/watch?v={link}", path, title)
            done = True
        except StopIteration:
            print(f"{title}: out of links, asking again")
            links = get_video_hrefs(title)
        except:
            pass


def extract_songs(res):
    for item in res["items"]:
        if(item["track"]["type"] == "track"): 
            yield (item["track"]["artists"][0]["name"], item["track"]["name"])

def get_songs(playlist_id):
    sess = requests.session()
    
    a = sess.get("https://open.spotify.com/get_access_token?reason=transport&productType=web_player")

    res = json.loads(a.text)
    token = res["accessToken"]
    
    bla = sess.get(f"https://api.spotify.com/v1/playlists/{playlist_id}/tracks?offset=0&limit=100",
             headers={"Authorization" : f"Bearer {token}"})
    
    bla = json.loads(bla.text)
    first = True
    
    while bla.get("next", False) or first:
        
        if first:
            first = False
            for artist, song in extract_songs(bla):
                yield f"{artist} - {song}"
        else:
            bla = sess.get(bla["next"], headers={"Authorization" : f"Bearer {token}"})
            bla = json.loads(bla.text)

            for artist, song in extract_songs(bla):
                yield f"{artist} - {song}"


class SongDownloader (threading.Thread):
    def __init__(self, title):
       threading.Thread.__init__(self)
       self.title = title

    def run(self):
        while(True):
            try:
                download_song_by_title(self.title)
                break
            except Exception as e:
                print(f"{self.title}: error, trying again")

    
def main():

    args = os.sys.argv[1:]
    spotify_playlist_id = args[0] if len(args) > 0 else input("enter a spotify playlist id to download (...from youtube...)\n")

    threads = []

    for full_name in get_songs(spotify_playlist_id):
        
        t = SongDownloader(full_name)
        t.start()
        
        threads.append(t)

    for thread in threads:
        thread.join()

if __name__ == "__main__":
    main()