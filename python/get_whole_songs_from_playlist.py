import requests
import json
from pprint import pprint

import youtube_download as myYT
#from pytube import YouTube

from threading import Thread


def extract_songs(res):
    for item in res["items"]:
        if(item["track"]["type"] == "track"): 
            yield (item["track"]["artists"][0]["name"], item["track"]["name"])

def get_songs(playlist_id):
    sess = requests.session()
    
    a = sess.get("https://open.spotify.com/access_token?reason=transport&productType=web_player")
    
    res = json.loads(a.text)
    token = res["accessToken"]
    
    bla = sess.get(f"https://api.spotify.com/v1/playlists/{playlist_id}/tracks?offset=0&limit=100",
             headers={"Authorization" : f"Bearer {token}"})

    
    bla = json.loads(bla.text)
    while bla.get("next", False):
        bla = sess.get(bla["next"], headers={"Authorization" : f"Bearer {token}"})
        bla = json.loads(bla.text)
        
        for artist, song in extract_songs(bla):
            yield f"{artist} - {song}"
  
  
def handle_song(song):
    title = f"https://www.youtube.com/watch?v={next(myYT.get_video_hrefs(song))}"
    
    yt = YouTube("https://www.youtube.com/watch?v=n06H7OcPd-g")
    print("blabla", yt.streams.filter(only_audio=True).first().stream_to_buffer().getvalue()[11300: 11380])
    
  
for song in get_songs("19nAL7akCCLuqln8nJlFa8"):
    print(song)
    #t = Thread(target = handle_song, args = (song, ))
    #t.start()
    #handle_song(song)
