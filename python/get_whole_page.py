from selenium import webdriver
from selenium.common.exceptions import StaleElementReferenceException, TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class wait_for_more_than_n_elements_to_be_present(object):
    def __init__(self, locator, count):
        self.locator = locator
        self.count = count

    def __call__(self, driver):
        try:
            elements = EC._find_elements(driver, self.locator)
            return len(elements) > self.count
        except StaleElementReferenceException:
            return False

def get_whole_page(url, class_indicator):
    driver = webdriver.Firefox(executable_path= r"C:\tools\geckodriver.exe")
    driver.maximize_window()
    driver.get(url)

    # initial wait for the tweets to load
    wait = WebDriverWait(driver, 10)
    wait.until(EC.visibility_of_element_located((By.CLASS_NAME, class_indicator)))

    # scroll down to the last tweet until there is no more tweets loaded
    while True:
        indicator = driver.find_element_by_class_name(class_indicator)
        number_of_indicators = len(indicator)

        driver.execute_script("arguments[0].scrollIntoView();", indicator[-1])

        try:
            wait.until(wait_for_more_than_n_elements_to_be_present((By.CLASS_NAME, class_indicator), number_of_indicators))
        except TimeoutException:
            break

    ret =  driver.page_source
    driver.close()
    return ret