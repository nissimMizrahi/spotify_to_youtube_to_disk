const fs = require('fs');
const ytdl = require('ytdl-core');
const axios = require("axios")

const jsdom = require("jsdom");
const { JSDOM } = jsdom;

const Spotify = require("./SpotifyApi")


let ytSearcher = axios.create({
    baseURL: `https://www.youtube.com/results`
})

function getSongHrefs(song) {

    return new Promise((resolve, reject) => {
        ytSearcher.get("/", {
            params: {
                search_query: song.replace(/ /g, "+")
            }
        }).then(res => {

            try {
                const { document } = (new JSDOM(res.data, {url: 'https://www.youtube.com'})).window;

                let ret = [...document.getElementsByClassName("yt-lockup-thumbnail contains-addto")].map(elem => /\/watch\?v=([^\"]+)/g.exec(elem.innerHTML)[1])
                resolve(ret)
            } catch (e) {
                getSongHrefs(song).then(res2 => resolve(res2))
            }

        }).catch(err => getSongHrefs(song).then(res => resolve(res)))
    })
}

function downloadSong(title) {
    getSongHrefs(title).then(hrefs => {
        console.log(title, hrefs[0])
    }).catch(err => console.log(`----------------------error ${title}-------------------------`))
}

Spotify().then(API => {
    API.getPlaylist(`19nAL7akCCLuqln8nJlFa8`).then(songs => {
        songs.map(song => downloadSong(`${song.artist} - ${song.name}`))
    })
})