const axios = require("axios")


class SpotifyApi {
    constructor(connection) {
        this.connection = connection
    }

    _getChunk(next) {
        return new Promise((resolve, reject) => {
            
            if (next) {
                this.connection.get(next).then(res => {
                    let songs = (res.data.items || [])
                    .filter(item => item.track.type == "track")
                    .map(item => {
                        return {
                            artist: item.track.artists[0].name,
                            name: item.track.name
                        } 
                    })

                    this._getChunk(res.data.next || null).then(res2 => {
                        resolve([...songs, ...res2])
                    }).catch(err => reject(err))
                }).catch(err => reject(err))
            }
            else {
                resolve([])
            }

        })
    }

    getPlaylist(playlist_id) {

        return new Promise((resolve, reject) => {
            let offset = 0
            let chunk_limit = 100
            this._getChunk(`https://api.spotify.com/v1/playlists/${playlist_id}/tracks?offset=${offset}&limit=${chunk_limit}`).then(res => {
                resolve(res)
            }).catch(err => reject(err))
        })

        
    }
}

module.exports = function new_SpotifyApi()
{
    return new Promise((resolve, reject) => {
        let connection = axios.create()

        connection.get("https://open.spotify.com/access_token", {
            reason: "transport",
            productType: "web_player"
        }).then(res => {
            connection.defaults.headers.common.Authorization = `Bearer ${res.data.accessToken}`
            resolve(new SpotifyApi(connection))
        }).catch(err => reject(err))
    })
}

